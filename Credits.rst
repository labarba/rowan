The following people contributed to the :py:mod:`rowan` package.

Vyas Ramasubramani <vramasub@umich.edu>, University of Michigan - **Lead developer**.

* Initial design
* Core quaternion operations
* Sphinx docs support
